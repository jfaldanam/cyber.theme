## Cyber.theme

cmus theme inspired in cyberpunk neon color palette

---

### Screenshots
![](img/cyber.theme_showcase.png)

---

### Installation
Depending from where you installed cmus your themes should be in one of these:
+ `~/.cmus` directory.
+ `~/.config/cmus` directory.
+ `/usr/share/cmus` directory.

Copy the `cyber.theme` file onto the one that contains the default themes.


Now open cmus and run the command `:colorscheme cyber`.

To go back to your previous theme use `:colorscheme ` and press tab until you find it.
